﻿using School.Infra.CrossCutting.Identity.Models;

namespace School.Infra.CrossCutting.Identity.Interfaces
{
    public interface ISecurityService
    {
        void Login(string userName, string userEmail, string password, bool rememberMe);
        void Register(ApplicationUser user, string password);
        void Logout();
        void ConfirmEmail(string userId, string code);        
    }
}
