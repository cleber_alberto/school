﻿$(function () {
    inicializarDataTable();
});

function inicializarDataTable() {
    var table = $("table.data-table").dataTable({
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "-All-"]],
        "responsive": true,
        "bLengthChange": true,
        "ordering": false,
        "scrollY": "45vh",
        "scrollCollapse": true,
        "paging": false,
        "searching": false//,
        //"language": {
        //    "url": "//cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json"
        //}
    });

    $("a[data-button='edit']").removeAttr("href");
    $("a[data-button='details']").removeAttr("href");

    $("table.data-table tbody").on("click", "tr", function () {
        if ($(this).hasClass("selected")) {
            $(this).removeClass("selected");

            $("a[data-button='edit']").removeAttr("href");
            $("a[data-button='details']").removeAttr("href");
        }
        else {
            table.$("tr.selected").removeClass("selected");
            $(this).addClass("selected");

            var href = $(this).data('href');
            $("a[data-button='edit']").attr('href', href);
            var hrefDetails = $(this).data('href-details');
            $("a[data-button='details']").attr('href', hrefDetails);
        }
    });
}

function ValidacoesEvento() {
    $.validator.methods.range = function (value, element, param) {
        var globalizedValue = value.replace(",", ".");
        return this.optional(element) || (globalizedValue >= param[0] && globalizedValue <= param[1]);
    }

    $.validator.methods.number = function (value, element) {
        return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:[\s\.,]\d{3})+)(?:[\.,]\d+)?$/.test(value);
    }

    toastr.options = {
        "closeButton": false,
        "debug": true,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    $("#DataInicio").datepicker({
        format: "dd/mm/yyyy",
        startDate: "tomorrow",
        language: "pt-BR",
        orientation: "bottom right",
        autoclose: true
    });

    $("#DataFim").datepicker({
        format: "dd/mm/yyyy",
        startDate: "tomorrow",
        language: "pt-BR",
        orientation: "bottom right",
        autoclose: true
    });

    // Validacoes de exibicao do endereco
    $(document).ready(function () {
        var $inputOnline = $("#Online");
        var $inputGratuito = $("#Gratuito");

        MostrarEndereco();
        MostrarValor();

        $inputOnline.click(function () {
            MostrarEndereco();
        });

        $inputGratuito.click(function () {
            MostrarValor();
        });

        function MostrarEndereco() {
            if ($inputOnline.is(":checked")) $("#EnderecoForm").hide();
            else $("#EnderecoForm").show();
        }

        function MostrarValor() {
            if ($inputGratuito.is(":checked")) {
                $("#Valor").prop("disabled", true);
            } else {
                $("#Valor").prop("disabled", false);
            }
        }
    });
}

function AjaxModal() {
    $(document).ready(function () {
        $(function () {
            $.ajaxSetup({ cache: false });

            $("a[data-modal]").on("click",
                function (e) {
                    $("#myModalContent").load(this.href,
                        function () {
                            $("#myModal").modal({
                                keyboard: true
                            },
                                "show");
                            bindForm(this);
                        });
                    return false;
                });
        });

        function bindForm(dialog) {
            $("form", dialog).submit(function () {
                $.ajax({
                    url: this.action,
                    type: this.method,
                    data: $(this).serialize(),
                    success: function (result) {
                        if (result.success) {
                            $("#myModal").modal("hide");
                            $("#EnderecoTarget").load(result.url); // Carrega o resultado HTML para a div demarcada
                        } else {
                            $("#myModalContent").html(result);
                            bindForm(dialog);
                        }
                    }
                });
                return false;
            });
        }
    });
}
