﻿using DDD.Shared.Notifications.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace School.Site.ViewComponents
{
    public class SummaryViewComponent : ViewComponent
    {
        private readonly INotification _notifications;

        public SummaryViewComponent(INotification notifications)
        {
            _notifications = notifications;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var notificacoes = await Task.FromResult(_notifications.Notifications);
            notificacoes.ForEach(c => ViewData.ModelState.AddModelError(string.Empty, c.Description));

            return View();
        }
    }
}
