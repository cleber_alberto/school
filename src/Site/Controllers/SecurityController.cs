﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDD.Shared.Notifications.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using School.Application.Interfaces;
using School.Application.ViewModels;

namespace School.Site.Controllers
{
    public class SecurityController : BaseController
    {
        private readonly INotification _notification;
        private readonly IIdentityService _service;

        public SecurityController(INotification notification, IIdentityService service) 
            : base(notification)
        {
            _notification = notification;
            _service = service;
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Login(IFormCollection collection)
        {
            var model = new LoginViewModel();

            if (!await TryUpdateModelAsync(model))
            {
                _notification.Add("Could not validate form.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _service.LoginAsync(model);
                    if (ValidOperation())
                        return RedirectToAction("Index", "Home");
                }
                catch (Exception exception)
                {
                    _notification.Add(exception.Message);
                }
            }

            return View(model);
        }

        [AllowAnonymous]
        [Route("register-user")]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("register-user")]
        public async Task<IActionResult> Register(IFormCollection collection)
        {
            var model = new RegisterViewModel();

            if (!await TryUpdateModelAsync(model))
            {
                _notification.Add("Could not validate form.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _service.Register(model);
                    if (ValidOperation())
                        return RedirectToAction("Login");
                }
                catch (Exception exception)
                {
                    _notification.Add(exception.Message);
                }
            }

            return View(model);
        }

        public async Task<IActionResult> Logout()
        {
            await _service.LogoutAsync();
            return RedirectToAction("Login");
        }
    }
}