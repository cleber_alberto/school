﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDD.Shared.Notifications.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using School.Application.Interfaces;
using School.Application.ViewModels;

namespace School.Site.Controllers
{
    public class TeacherController : BaseController
    {
        private readonly INotification _notification;
        private readonly ITeacherService _service;

        public TeacherController(INotification notification, ITeacherService service) 
            : base(notification)
        {
            _notification = notification;
            _service = service;
        }

        public async Task<IActionResult> Index()
        {
            var model = await _service.ListAsync();
            return View(model);
        }

        public async Task<IActionResult> Details(Guid id)
        {
            var model = await _service.GetAsync(id);
            return View(model);
        }

        public async Task<IActionResult> Edit(Guid? id)
        {
            var model = id == null
                ? new TeacherViewModel()
                : await _service.GetAsync(id.Value);

            await LoadViewData();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(IFormCollection collection)
        {
            var model = new TeacherViewModel();
            if (!await TryUpdateModelAsync(model))
                _notification.Add("There was an error while trying to validate the form.");

            if (ModelState.IsValid)
            {
                try
                {
                    if (model.Id == Guid.Empty)
                        _service.Add(model);
                    else
                        _service.Update(model);

                    if(ValidOperation())
                        return RedirectToAction("Index");
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }

            await LoadViewData();
            return View(model);
        }

        private async Task LoadViewData()
        {
            ViewBag.Courses = new SelectList(await _service.ListCourseAsync(), "Id", "Name");
        }
    }
}