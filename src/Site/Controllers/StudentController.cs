﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDD.Shared.Notifications.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using School.Application.Interfaces;
using School.Application.ViewModels;

namespace School.Site.Controllers
{
    public class StudentController : BaseController
    {
        private readonly INotification _notification;
        private readonly IStudentService _service;

        public StudentController(INotification notification, IStudentService service) 
            : base(notification)
        {
            _notification = notification;
            _service = service;
        }

        public async Task<IActionResult> Index()
        {
            var model = await _service.ListAsync();
            return View(model);
        }

        public async Task<IActionResult> Details(Guid id)
        {
            var model = await _service.GetAsync(id);
            return View(model);
        }

        public async Task<IActionResult> Edit(Guid? id)
        {
            var model = id == null
                ? new StudentViewModel()
                : await _service.GetAsync(id.Value);

            TempData["Courses"] = JsonConvert.SerializeObject(model.Courses);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(IFormCollection collection)
        {
            var model = new StudentViewModel();

            if (!await TryUpdateModelAsync(model))
                _notification.Add("There was an error while trying to validate the form.");

            if (ModelState.IsValid)
            {
                try
                {
                    model.Courses = JsonConvert.DeserializeObject<List<CourseViewModel>>(TempData.Peek("Courses") as string);

                    if (model.Id == Guid.Empty)
                        _service.Add(model);
                    else
                        _service.Update(model);

                    if(ValidOperation())
                        return RedirectToAction("Index");
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }

            return View(model);
        }

        public IActionResult FindCourses()
        {
            var model = JsonConvert.DeserializeObject<List<CourseViewModel>>(TempData.Peek("Courses") as string);
            return PartialView("_ListCourses", model);
        }

        public async Task<IActionResult> ListCourses()
        {
            var model = await _service.ListCoursesAsync();
            return PartialView("_Courses", model);
        }

        [HttpPost]
        public async Task<IActionResult> AddCourse(Guid id)
        {
            var courses = JsonConvert.DeserializeObject<List<CourseViewModel>>(TempData.Peek("Courses") as string);

            if (!courses.Any(p => p.Id == id))
            {
                var course = await _service.GetCoursesAsync(id);
                courses.Add(course);
                TempData["Courses"] = JsonConvert.SerializeObject(courses);
                return Json(new { result = true });
            }            

            return Json(new { result = false });
        }

        [HttpPost]
        public IActionResult RemoveCourse(Guid id)
        {
            var courses = JsonConvert.DeserializeObject<List<CourseViewModel>>(TempData.Peek("Courses") as string);
            var course = courses.FirstOrDefault(p => p.Id == id);

            if (course != null)
            {                
                courses.Remove(course);
                TempData["Courses"] = JsonConvert.SerializeObject(courses);
                return Json(new { result = true });
            }

            return Json(new { result = false });
        }
    }
}