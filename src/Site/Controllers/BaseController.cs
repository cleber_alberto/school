﻿using DDD.Shared.Notifications.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace School.Site.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        private readonly INotification _notification;

        public BaseController(INotification notification)
        {
            _notification = notification;
        }

        protected bool ValidOperation()
        {
            return (!_notification.HasNotifications);
        }

        internal void Notificar()
        {
            _notification.Notifications.ForEach(n => ModelState.AddModelError(string.Empty, n.GetNotification()));
        }
    }
}