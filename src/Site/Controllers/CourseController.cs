﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDD.Shared.Notifications.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using School.Application.Interfaces;
using School.Application.ViewModels;

namespace School.Site.Controllers
{
    public class CourseController : BaseController
    {
        private readonly INotification _notification;
        private readonly ICourseService _service;

        public CourseController(INotification notification, ICourseService service) : base(notification)
        {
            _service = service;
        }

        public async Task<IActionResult> Index()
        {
            var model = await _service.ListAsync();
            return View(model);
        }

        public async Task<IActionResult> Details(Guid id)
        {
            var model = await _service.GetAsync(id);
            return View(model);
        }

        public async Task<IActionResult> Edit(Guid? id)
        {
            var model = id == null
                ? new CourseViewModel()
                : await _service.GetAsync(id.Value);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(IFormCollection collection)
        {
            var model = new CourseViewModel();
            if (!await TryUpdateModelAsync(model))
                _notification.Add("There was an error while trying to validate the form.");

            if (ModelState.IsValid)
            {
                try
                {
                    if (model.Id == Guid.Empty)
                        _service.Add(model);
                    else
                        _service.Update(model);

                    if(ValidOperation())
                        return RedirectToAction("Index");
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }

            return View(model);
        }
    }
}