﻿using AutoMapper;
using DDD.Shared.CQRS;
using DDD.Shared.CQRS.Interfaces;
using DDD.Shared.Interfaces;
using DDD.Shared.Notifications;
using DDD.Shared.Notifications.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using School.Application.AutoMapper;
using School.Application.Interfaces;
using School.Application.Services;
using School.Domain.Commands;
using School.Infra.CrossCutting.Identity.Data;
using School.Infra.CrossCutting.Identity.IoC;
using School.Infra.Data.DbContexts;
using School.Infra.Data.Repositories;
using School.Infra.Data.UoW;

namespace School.Infra.CrossCutting.IoC
{
    public class IoCConfig
    {

        public static void ConfigureDevelopment(IConfiguration configuration, IServiceCollection services)
        {
            // use in-memory database
            services.AddDbContext<SchoolDbContext>(opt => opt.UseInMemoryDatabase("SchoolDbConnection"));
            services.AddDbContext<ApplicationDbContext>(opt => opt.UseInMemoryDatabase("IdentityDbConnection"));

            ConfigureServices(configuration, services);
        }

        public static void ConfigureProdution(IConfiguration configuration, IServiceCollection services)
        {
            services
                .AddEntityFrameworkSqlServer()
                .AddDbContext<SchoolDbContext>(opt => opt.UseSqlServer(configuration.GetConnectionString("SchoolDbConnection")));
            services
                .AddEntityFrameworkSqlServer()
                .AddDbContext<ApplicationDbContext>(opt => opt.UseSqlServer(configuration.GetConnectionString("IdentityDbConnection")));

            ConfigureServices(configuration, services);
        }

        public static void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            services.AddScoped<INotification, Notification>()
                .BuildServiceProvider();

            // Autommapper
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile(new SchoolProfile());
            });
            
            // Persistance
            services.AddScoped<IDbContext, SchoolDbContext>();

            // Bus
            services.AddScoped<IBus, Bus>();

            // Identity
            IdentityBootstrapper.RegisterServices(configuration, services);

            // Infrastructure
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IRepositoryAsync<>), typeof(Repository<>));

            // Services
            services.AddScoped<IIdentityService, IdentityService>();
            services.AddScoped<ICourseService, CourseService>();
            services.AddScoped<ITeacherService, TeacherService>();
            services.AddScoped<IStudentService, StudentService>();

            // Commands
            services.AddScoped<IHandler<TeacherAddCommand>, TeacherCommandHandler>();
            services.AddScoped<IHandler<TeacherUpdateCommand>, TeacherCommandHandler>();

            services.AddScoped<IHandler<StudentAddCommand>, StudentCommandHandler>();
            services.AddScoped<IHandler<StudentUpdateCommand>, StudentCommandHandler>();
        }
    }
}
