﻿using DDD.Shared.Interfaces;
using DDD.Shared.Models;
using DDD.Shared.Validations;
using School.Domain.Models.Validations;
using System;
using System.Collections.Generic;

namespace School.Domain.Models
{
    public class Teacher : BaseEntity, IValidatable<Teacher>
    {
        public string Name { get; set; }
        public int Classroom { get; set; }
        public Guid CourseId { get; set; }
        public Course Course { get; set; }

        #region ... Validation ...
        public bool Validate(IValidator<Teacher> validator, out IEnumerable<string> brokenRules)
        {
            brokenRules = validator.BrokenRules(this);
            return validator.IsValid(this);
        }

        public ValidationResult Validation()
        {
            var validationResult = new ValidationResult();

            var validator = new TeacherValidation();
            var isValid = this.Validate(validator, out IEnumerable<string> brokenRules);

            if (!isValid)
            {
                foreach (var item in brokenRules) { validationResult.AddError(item); }
            }

            return validationResult;
        }
        #endregion

        #region ... Factory ...
        public static Teacher Create(string name, int classroom, Guid courseId)
        {
            return new Teacher() { Id = Guid.NewGuid(), Name = name, Classroom = classroom, CourseId = courseId };
        }
        #endregion
    }
}
