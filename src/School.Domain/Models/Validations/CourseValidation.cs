﻿using DDD.Shared.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace School.Domain.Models.Validations
{
    public class CourseValidation : IValidator<Course>
    {
        public IEnumerable<string> BrokenRules(Course entity)
        {
            if (string.IsNullOrEmpty(entity.Name))
                yield return "Name is required.";

            yield break;
        }

        public bool IsValid(Course entity) => !BrokenRules(entity).Any();
    }
}
