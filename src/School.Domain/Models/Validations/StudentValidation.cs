﻿using DDD.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace School.Domain.Models.Validations
{
    public class StudentValidation : IValidator<Student>
    {
        public IEnumerable<string> BrokenRules(Student entity)
        {
            if (string.IsNullOrEmpty(entity.Name))
                yield return "Name is required.";

            if (entity.BirthDate == null)
                yield return "Birth date is required.";

            if (string.IsNullOrEmpty(entity.Address))
                yield return "Address is required.";

            if (string.IsNullOrEmpty(entity.Email))
                yield return "E-mail is required.";

            yield break;
        }

        public bool IsValid(Student entity) => !BrokenRules(entity).Any();
    }
}
