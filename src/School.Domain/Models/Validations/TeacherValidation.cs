﻿using DDD.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace School.Domain.Models.Validations
{
    public class TeacherValidation : IValidator<Teacher>
    {
        public IEnumerable<string> BrokenRules(Teacher entity)
        {
            if (string.IsNullOrEmpty(entity.Name))
                yield return "Name is required.";

            if (entity.Classroom <= 0)
                yield return "Classroom number is required.";

            if (entity.CourseId == Guid.Empty)
                yield return "Course is required.";

            yield break;
        }

        public bool IsValid(Teacher entity) => !BrokenRules(entity).Any();
    }
}
