﻿using DDD.Shared.Specifications;

namespace School.Domain.Models.Specifications
{
    public class TeacherFilterSpecification : BaseSpecification<Teacher>
    {
        public TeacherFilterSpecification() 
            : base(f => !f.IsDeleted)
        {
            AddInclude(i => i.Course);
        }
    }
}
