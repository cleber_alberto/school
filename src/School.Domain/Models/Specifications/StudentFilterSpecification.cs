﻿using DDD.Shared.Specifications;
using System;

namespace School.Domain.Models.Specifications
{
    public class StudentFilterSpecification : BaseSpecification<Student>
    {
        public StudentFilterSpecification()
            : base(f => !f.IsDeleted)
        {
        }

        public StudentFilterSpecification(Guid id)
            : base(f => f.Id == id && !f.IsDeleted)
        {
            AddInclude(i => i.Courses);
            AddInclude("Courses.Course");
        }
    }
}
