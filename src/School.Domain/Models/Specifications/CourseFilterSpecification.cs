﻿using DDD.Shared.Specifications;

namespace School.Domain.Models.Specifications
{
    public class CourseFilterSpecification : BaseSpecification<Course>
    {
        public CourseFilterSpecification()
            : base(f => !f.IsDeleted)
        {
        }
    }
}
