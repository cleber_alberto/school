﻿using DDD.Shared.Interfaces;
using DDD.Shared.Models;
using DDD.Shared.Validations;
using School.Domain.Models.Validations;
using System;
using System.Collections.Generic;

namespace School.Domain.Models
{
    public class Course : BaseEntity, IValidatable<Course>
    {
        public string Name { get; set; }

        public virtual ICollection<Teacher> Teachers { get; set; }
        public virtual ICollection<StudentCourse> Students { get; set; }

        #region ... Validation ...
        public bool Validate(IValidator<Course> validator, out IEnumerable<string> brokenRules)
        {
            brokenRules = validator.BrokenRules(this);
            return validator.IsValid(this);
        }

        public ValidationResult Validation()
        {
            var validationResult = new ValidationResult();

            var validator = new CourseValidation();
            var isValid = this.Validate(validator, out IEnumerable<string> brokenRules);

            if (!isValid)
            {
                foreach (var item in brokenRules) { validationResult.AddError(item); }
            }

            return validationResult;
        }
        #endregion

        #region ... Factory ...
        public static Course Create(string name)
        {
            return new Course() { Id = Guid.NewGuid(), Name = name };
        }
        #endregion
    }
}
