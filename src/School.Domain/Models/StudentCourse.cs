﻿using System;

namespace School.Domain.Models
{
    public class StudentCourse
    {
        public Guid StudentId { get; set; }
        public Guid CourseId { get; set; }

        public virtual Student Student { get; set; }
        public virtual Course Course { get; set; }

        #region ... Factory ...
        public static StudentCourse Create(Guid studentId, Guid courseId)
        {
            return new StudentCourse() { StudentId = studentId, CourseId = courseId };
        }
        #endregion
    }
}
