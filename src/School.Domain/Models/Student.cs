﻿using System;
using System.Collections.Generic;
using DDD.Shared.Interfaces;
using DDD.Shared.Models;
using DDD.Shared.Validations;
using School.Domain.Models.Validations;

namespace School.Domain.Models
{
    public class Student : BaseEntity, IValidatable<Student>
    {
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public virtual ICollection<StudentCourse> Courses { get; private set; }

        public void AddCourse(StudentCourse course)
        {
            if (this.Courses == null)
                this.Courses = new List<StudentCourse>();

            this.Courses.Add(course);
        }

        public void RemoveCourse(StudentCourse course)
        {
            this.Courses.Remove(course);
        }

        #region ... Validation ...
        public bool Validate(IValidator<Student> validator, out IEnumerable<string> brokenRules)
        {
            brokenRules = validator.BrokenRules(this);
            return validator.IsValid(this);
        }

        public ValidationResult Validation()
        {
            var validationResult = new ValidationResult();

            var validator = new StudentValidation();
            var isValid = this.Validate(validator, out IEnumerable<string> brokenRules);

            if (!isValid)
            {
                foreach (var item in brokenRules) { validationResult.AddError(item); }
            }

            return validationResult;
        }
        #endregion

        #region ... Factory ...
        public static Student Create(string name, DateTime birthDate, string address, string email)
        {
            return new Student() { Id = Guid.NewGuid(), Name = name, BirthDate = birthDate, Address = address, Email = email };
        }
        #endregion
    }
}
