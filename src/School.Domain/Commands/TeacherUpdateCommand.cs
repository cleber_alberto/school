﻿using DDD.Shared.CQRS;
using System;

namespace School.Domain.Commands
{
    public class TeacherUpdateCommand : Command
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Classroom { get; set; }
        public Guid CourseId { get; set; }
    }
}
