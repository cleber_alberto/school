﻿using DDD.Shared.CQRS.Handlers;
using DDD.Shared.CQRS.Interfaces;
using DDD.Shared.Interfaces;
using DDD.Shared.Notifications.Interfaces;
using School.Domain.Models;
using School.Domain.Models.Specifications;
using System.Linq;

namespace School.Domain.Commands
{
    public class StudentCommandHandler : CommandHandler
        , IHandler<StudentAddCommand>
        , IHandler<StudentUpdateCommand>
    {
        private readonly INotification _notification;
        private readonly IRepository<Student> _repository;

        public StudentCommandHandler(
            IBus bus, 
            INotification notification, 
            IRepository<Student> repository) 
            : base(bus, notification)
        {
            _notification = notification;
            _repository = repository;
        }

        public void Handle(StudentAddCommand message)
        {
            var student = Student.Create(message.Name, message.BirthDate, message.Address, message.Email);
            foreach (var course in message.Courses)
            {
                student.AddCourse(StudentCourse.Create(student.Id, course.Id));
            }

            var validationResult = student.Validation();
            if (!validationResult.IsValid)
            {
                Notifications(validationResult);
                return;
            }

            _repository.Add(student);
        }

        public void Handle(StudentUpdateCommand message)
        {
            var student = _repository.List(new StudentFilterSpecification(message.Id)).FirstOrDefault();
            if (student == null)
            {
                _notification.Add("Student not found.");
                return;
            }

            student.Name = message.Name;
            student.BirthDate = message.BirthDate;
            student.Address = message.Address;
            student.Email = message.Email;

            if (message.Courses == null)
            {
                foreach (var course in student.Courses)
                {
                    student.RemoveCourse(course);
                }
            }
            else
            {
                foreach (var course in message.Courses)
                {
                    var studentCourse = student.Courses.FirstOrDefault(p => p.CourseId == course.Id);
                    if (studentCourse == null)
                    {
                        student.AddCourse(StudentCourse.Create(student.Id, course.Id));
                    }
                }

                foreach (var course in student.Courses.ToList())
                {
                    if(!message.Courses.Any(p => p.Id == course.CourseId))
                        student.RemoveCourse(course);
                }
            }

            var validationResult = student.Validation();
            if (!validationResult.IsValid)
            {
                Notifications(validationResult);
                return;
            }

            _repository.Update(student);
        }
    }
}
