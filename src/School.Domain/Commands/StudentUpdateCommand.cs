﻿using DDD.Shared.CQRS;
using School.Domain.Models;
using System;
using System.Collections.Generic;

namespace School.Domain.Commands
{
    public class StudentUpdateCommand : Command
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public List<Course> Courses { get; set; }
    }
}
