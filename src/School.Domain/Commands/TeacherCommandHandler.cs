﻿using DDD.Shared.CQRS.Handlers;
using DDD.Shared.CQRS.Interfaces;
using DDD.Shared.Interfaces;
using DDD.Shared.Notifications.Interfaces;
using School.Domain.Models;

namespace School.Domain.Commands
{
    public class TeacherCommandHandler : CommandHandler
        , IHandler<TeacherAddCommand>
        , IHandler<TeacherUpdateCommand>   
    {
        private readonly INotification _notification;
        private readonly IRepository<Teacher> _repository;
        private readonly IRepository<Course> _repositoryCourse;

        public TeacherCommandHandler(
            IBus bus, 
            INotification notification, 
            IRepository<Teacher> repository, 
            IRepository<Course> repositoryCourse) 
            : base(bus, notification)
        {
            _notification = notification;
            _repository = repository;
            _repositoryCourse = repositoryCourse;
        }

        public void Handle(TeacherAddCommand message)
        {
            if(_repositoryCourse.GetById(message.CourseId) == null)
            {
                _notification.Add("Course not found.");
                return;
            }

            var teacher = Teacher.Create(message.Name, message.Classroom, message.CourseId);
            var validationResult = teacher.Validation();
            if (!validationResult.IsValid)
            {
                Notifications(validationResult);
                return;
            }

            _repository.Add(teacher);
        }

        public void Handle(TeacherUpdateCommand message)
        {
            var teacher = _repository.GetById(message.Id);
            if (teacher == null)
            {
                _notification.Add("Teacher not found.");
                return;
            }

            if (_repositoryCourse.GetById(message.CourseId) == null)
            {
                _notification.Add("Course not found.");
                return;
            }

            teacher.Name = message.Name;
            teacher.Classroom = message.Classroom;
            teacher.CourseId = message.CourseId;

            var validationResult = teacher.Validation();
            if (!validationResult.IsValid)
            {
                Notifications(validationResult);
                return;
            }

            _repository.Update(teacher);
        }
    }
}
