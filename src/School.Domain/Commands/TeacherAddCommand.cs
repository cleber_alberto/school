﻿using DDD.Shared.CQRS;
using System;

namespace School.Domain.Commands
{
    public class TeacherAddCommand : Command
    {
        public string Name { get; set; }
        public int Classroom { get; set; }
        public Guid CourseId { get; set; }
    }
}
