﻿using DDD.Shared.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Shared.Interfaces
{
    public interface IRepositoryAsync<T> where T : BaseEntity
    {
        Task<T> GetByIdAsync(Guid id);
        Task<List<T>> ListAllAsync();
        Task<List<T>> ListAsync(ISpecification<T> spec);
        Task<T> AddAsync(T entity);
    }
}
