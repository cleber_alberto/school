﻿using DDD.Shared.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Shared.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        T GetById(Guid id);
        Task<T> GetByIdAsync(Guid id);
        T GetSingleBySpec(ISpecification<T> spec);

        IEnumerable<T> ListAll();
        Task<IEnumerable<T>> ListAllAsync();
        
        IEnumerable<T> List(ISpecification<T> spec);
        Task<IEnumerable<T>> ListAsync(ISpecification<T> spec);

        T Add(T entity);
        Task<T> AddAsync(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
