﻿using System.Collections.Generic;

namespace DDD.Shared.Interfaces
{
    public interface IValidator<T>
    {
        bool IsValid(T entity);
        IEnumerable<string> BrokenRules(T entity);
    }
}
