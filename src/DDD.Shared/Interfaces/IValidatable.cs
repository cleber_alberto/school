﻿using System.Collections.Generic;

namespace DDD.Shared.Interfaces
{
    public interface IValidatable<T>
    {
        bool Validate(IValidator<T> validator, out IEnumerable<string> brokenRules);
    }

    //public class Order : IValidatable<Order>
    //{
    //    public int Id { get; set; }
    //    public string Customer { get; set; }

    //    public bool Validate(IValidator<Order> validator, out IEnumerable<string> brokenRules)
    //    {
    //        brokenRules = validator.BrokenRules(this);
    //        return validator.IsValid(this);
    //    }
    //}

    //public class OrderPersistenceValidator : IValidator<Order>
    //{
    //    public bool IsValid(Order entity)
    //    {
    //        return BrokenRules(entity).Count() > 0;
    //    }

    //    public IEnumerable<string> BrokenRules(Order entity)
    //    {
    //        if (entity.Id < 0)
    //            yield return "Id cannot be less than 0.";

    //        if (string.IsNullOrEmpty(entity.Customer))
    //            yield return "Must include a customer.";

    //        yield break;
    //    }
    //}

    //Order order = new Order();
    //OrderPersistenceValidator validator = new OrderPersistenceValidator();

    //IEnumerable<string> brokenRules;
    //bool isValid = order.Validate(validator, out brokenRules);

}
