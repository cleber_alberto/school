﻿using System;

namespace DDD.Shared.Interfaces
{
    public interface IAggregateRoot
    {
        Guid Id { get; }
    }
}
