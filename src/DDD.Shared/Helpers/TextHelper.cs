﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace DDD.Shared.Helpers
{
    public static class TextHelper
    {
        public static string RemoverAcentos(string texto)
        {
            const string comAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç";
            const string semAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc";

            for (var i = 0; i < comAcentos.Length; i++)
            {
                texto = texto.Replace(comAcentos[i].ToString(), semAcentos[i].ToString());
            }
            return texto;
        }

        public static string SomenteNumeros(string texto)
        {
            return string.IsNullOrEmpty(texto) 
                ? texto 
                : Regex.Replace(texto, "[^0-9]+", "");
        }

        public static bool ValidarFormatoEmail(string email)
        {
            var rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");
            return rg.IsMatch(email);
        }

        public static string MontarUrlAmigavel(string url)
        {
            return RemoverAcentos(Regex.Replace(url.TrimEnd().ToLower().Replace(' ', '-'), "[^\\w\\d-]+", ""));
        }

        //public static string XmlSerializer<T>(T value)
        //{
        //    if (value == null)
        //    {
        //        return string.Empty;
        //    }
        //    try
        //    {
        //        var xmlserializer = new XmlSerializer(typeof(T));
        //        var stringWriter = new StringWriter();
        //        using (var writer = XmlWriter.Create(stringWriter))
        //        {
        //            xmlserializer.Serialize(writer, value);
        //            return stringWriter.ToString();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("XML: Serialização - um erro ocorreu.", ex);
        //    }
        //}
    }
}
