﻿using DDD.Shared.CQRS.Interfaces;
using DDD.Shared.Notifications.Interfaces;
using System;
using System.Linq;

namespace DDD.Shared.CQRS
{
    public sealed class Bus : IBus
    {
        private static IServiceProvider _services;
        private readonly INotification _notification;

        public Bus(IServiceProvider services, INotification notification)
        {
            _services = services;
            _notification = notification;
        }

        public void SendCommands<TCommand>(params TCommand[] commands) where TCommand : ICommand
        {
            try
            {
                if (commands == null || !commands.Any())
                    return;

                foreach (var theCommand in commands)
                {
                    Publish(theCommand);
                }
            }
            catch (Exception exception)
            {
                _notification.Add(exception.Message);
            }
        }

        public void RaiseEvents<TEvent>(params TEvent[] events) where TEvent : IEvent
        {
            if (events == null || !events.Any())
                return;

            foreach (var theEvent in events)
            {
                Publish(theEvent);
            }
        }              

        public TResponse SendRequest<TResponse>(IRequest<TResponse> theRequest) where TResponse : IResponse
        {
            throw new NotImplementedException();
        }

        private static void Publish<TEvent>(TEvent theEvent)
                    where TEvent : IMessage
        {
            if (_services == null)
                throw new ArgumentNullException("services");

            var obj = _services.GetService(typeof(IHandler<TEvent>));
            if (obj == null)
                throw new ArgumentNullException("event");

            ((IHandler<TEvent>)obj).Handle(theEvent);
        }
    }
}
