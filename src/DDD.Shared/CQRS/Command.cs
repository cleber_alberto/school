﻿using DDD.Shared.CQRS.Interfaces;

namespace DDD.Shared.CQRS
{
    public abstract class Command : Message, ICommand
    {
    }
}
