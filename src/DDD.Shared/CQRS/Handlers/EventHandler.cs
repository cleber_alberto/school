﻿using DDD.Shared.CQRS.Interfaces;

namespace DDD.Shared.CQRS.Handlers
{
    public class EventHandler
    {
        private readonly IBus _bus;
        private readonly IDomainEventRepository _repository;

        public EventHandler(IBus bus, IDomainEventRepository repository)
        {
            _bus = bus;
            _repository = repository;
        }
    }
}
