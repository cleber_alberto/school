﻿using DDD.Shared.CQRS.Interfaces;
using DDD.Shared.Interfaces;
using DDD.Shared.Notifications.Interfaces;
using DDD.Shared.Validations;

namespace DDD.Shared.CQRS.Handlers
{
    public class CommandHandler
    {
        private readonly IBus _bus;
        private readonly INotification _notification;

        public CommandHandler(
            IBus bus,            
            INotification notification)
        {
            _bus = bus;            
            _notification = notification;
        }

        protected void Notifications(ValidationResult validationResult)
        {
            foreach (var error in validationResult.Erros)
            {
                _notification.Add(error.ErrorMessage);
            }
        }
    }
}
