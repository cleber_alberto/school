﻿using DDD.Shared.CQRS;
using DDD.Shared.CQRS.Interfaces;
using DDD.Shared.Interfaces;
using DDD.Shared.Notifications.Interfaces;
using DDD.Shared.Validations;

namespace DDD.Shared.CQRS.Handlers
{
    public abstract class Handler<TMessage> : IHandler<TMessage> where TMessage : Message
    {
        private readonly IBus _bus;
        private readonly IUnitOfWork _uow;
        private readonly INotification _notification;

        public Handler(
            IBus bus,
            IUnitOfWork uow,
            INotification notification)
        {
            _bus = bus;
            _uow = uow;
            _notification = notification;
        }

        public virtual void Handle(TMessage @message)
        {
            //@message.Flatten();
            //_domainEventRepository.Add(@message);
        }

        protected void NotificarValidacoesErro(ValidationResult validationResult)
        {
            foreach (var error in validationResult.Erros)
            {
                _notification.Add(error.ErrorMessage);
                //_bus.RaiseEvent(new DomainNotification("Erro", error.ErrorMessage));
            }
        }

        protected bool Commit()
        {
            if (_notification.HasNotifications) return false;
            var commandResponse = _uow.Commit();
            if (commandResponse.Success) return true;

            //_bus.RaiseEvent(new DomainNotification("Commit", "Ocorreu um erro ao salvar os dados no banco"));
            return false;
        }
    }
}
