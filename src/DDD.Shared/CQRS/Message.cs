﻿using DDD.Shared.CQRS.Interfaces;
using System;
using System.Collections.Generic;

namespace DDD.Shared.CQRS
{
    public abstract class Message : IMessage
    {
        public Guid AggregateId { get; set; }
        public string UserId { get; set; }
        public DateTime Created { get; private set; }
        public string Type { get; private set; }
        protected Dictionary<string, Object> Args { get; private set; }        

        public Message()
        {
            Type = GetType().Name;
            Created = DateTime.Now;
            Args = new Dictionary<string, object>();
        }
    }
}
