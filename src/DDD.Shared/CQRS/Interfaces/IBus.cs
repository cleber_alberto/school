﻿
using DDD.Shared.CQRS;

namespace DDD.Shared.CQRS.Interfaces
{
    public interface IBus
    {
        void SendCommands<TCommand>(params TCommand[] commands) where TCommand : ICommand;
        void RaiseEvents<TEvent>(params TEvent[] events) where TEvent : IEvent;
        TResponse SendRequest<TResponse>(IRequest<TResponse> theRequest) where TResponse : IResponse;
    }
}
