﻿namespace DDD.Shared.CQRS.Interfaces
{
    public interface IRequest<TResponse> : IMessage where TResponse : IResponse
    {
    }
}
