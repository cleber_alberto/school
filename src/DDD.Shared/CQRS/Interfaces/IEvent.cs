﻿namespace DDD.Shared.CQRS.Interfaces
{
    public interface IEvent : IMessage
    {
        void Flatten();
    }
}
