﻿using DDD.Shared.CQRS;
using System.Collections.Generic;

namespace DDD.Shared.CQRS.Interfaces
{
    public interface IDomainNotificationHandler<T> 
        : IHandler<T> where T : Message
    {
        bool HasNotifications();
        List<T> GetNotifications();
    }
}
