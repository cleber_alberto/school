﻿using DDD.Shared.CQRS;
using System.Collections.Generic;

namespace DDD.Shared.CQRS.Interfaces
{
    public interface IDomainEventRepository
    {
        void Add<TDomainEvent>(TDomainEvent domainEvent) where TDomainEvent : IEvent;
        IEnumerable<DomainEventRecord> FindAll();
    }
}
