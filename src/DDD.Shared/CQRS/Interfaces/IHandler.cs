﻿using DDD.Shared.CQRS;

namespace DDD.Shared.CQRS.Interfaces
{
    public interface IHandler<in T> where T : IMessage
    {
        void Handle(T message);
    }
}
