﻿using DDD.Shared.CQRS.Interfaces;

namespace DDD.Shared.CQRS
{
    public abstract class Request<TResponse> : Message, IRequest<TResponse> where TResponse : IResponse
    {
    }
}
