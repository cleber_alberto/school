﻿using System;
using System.Collections.Generic;
using DDD.Shared.CQRS.Interfaces;

namespace DDD.Shared.CQRS
{
    public abstract class Event : Message, IEvent
    {
        protected new Dictionary<string, Object> Args { get; private set; }

        public abstract void Flatten();
    }
}
