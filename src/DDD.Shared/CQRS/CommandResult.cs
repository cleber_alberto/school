﻿namespace DDD.Shared.CQRS
{
    public class CommandResult
    {
        public bool Success { get; private set; }
        public static CommandResult Ok = new CommandResult() { Success = true };
        public static CommandResult Fail = new CommandResult() { Success = false };

        public CommandResult(bool success = false)
        {
            Success = success;
        }
    }
}
