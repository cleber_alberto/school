﻿using System;
using System.Collections.Generic;

namespace DDD.Shared.CQRS
{
    public class DomainEventRecord
    {
        public string Type { get; set; }
        public string Args { get; set; }
        public Guid AggregateId { get; set; }
        public DateTime Created { get; set; }
        public string UserId { get; set; }
    }
}
