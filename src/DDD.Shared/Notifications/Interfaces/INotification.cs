﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Shared.Notifications.Interfaces
{
    public interface INotification
    {
        List<Message> Notifications { get; }
        bool HasNotifications { get; }
        bool HasErrorsNotifications { get; }
        void Add(string message);
        Task AddAsync(string message);
        void Add(TypeMessage type, string message);
    }
}
