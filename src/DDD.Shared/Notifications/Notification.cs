﻿using DDD.Shared.Notifications.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace DDD.Shared.Notifications
{
    public enum TypeMessage
    {
        Undefined,
        Information,
        Alert,
        Warning,
        Error,
    }

    public struct Message
    {
        public string Description { get; set; }
        public TypeMessage Type { get; set; }

        public string GetNotification()
        {
            return this.Description;
        }

        public string GetTypeAndNotification()
        {
            return $"Type {this.Type} - {this.Description}";
        }
    }

    public class Notification : INotification
    {
        public List<Message> Notifications { get; private set; }        

        public bool HasNotifications => Notifications.Count != 0;
        public bool HasErrorsNotifications => Notifications.Count(t => t.Type == TypeMessage.Undefined || t.Type == TypeMessage.Error) != 0;

        public Notification()
        {
            Notifications = new List<Message>();
        }

        public void Add(string message)
        {
            this.Notifications.Add(new Message { Type = TypeMessage.Undefined, Description = message });
        }

        public void Add(TypeMessage type, string message)
        {
            this.Notifications.Add(new Message { Type = type, Description = message });
        }

        public async Task AddAsync(string message)
        {
            Task addTask = new Task(() => this.Add(message));
            await addTask;
        }
    }
}
