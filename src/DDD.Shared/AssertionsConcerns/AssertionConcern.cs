﻿using DDD.Shared.Validations.Interfaces;
using System.Text.RegularExpressions;

namespace DDD.Shared.AssertionsConcerns
{
    public class AssertionConcern
    {
        private static IValidationResult _validationResult;

        protected AssertionConcern(IValidationResult validationResult)
        {
            _validationResult = validationResult;
        }

        public static void AssertArgumentEquals(object object1, object object2, string message)
        {
            if(object1 != object2)
                _validationResult.AddError(message);
        }

        public static void AssertArgumentFalse(bool boolValue, string message)
        {
            if (boolValue)
                _validationResult.AddError(message);
        }

        public void AssertArgumentLength(string stringValue, int maximum, string message)
        {
            if (stringValue == null)
                stringValue = string.Empty;

            var length = stringValue.Trim().Length;
            if(length > maximum)
                _validationResult.AddError(message);
        }

        public static void AssertArgumentLength(string stringValue, int minimum, int maximum, string message)
        {
            if (stringValue == null)
                stringValue = string.Empty;

            var length = stringValue.Trim().Length;
            if (length < minimum || length > maximum)
            {
                _validationResult.AddError(message);
            }
        }

        public static void AssertArgumentMatches(string pattern, string stringValue, string message)
        {
            if (stringValue == null)
                stringValue = string.Empty;

            var regex = new Regex(pattern);

            if (!regex.IsMatch(stringValue))
            {
                _validationResult.AddError(message);
            }
        }

        public static void AssertArgumentNotIsNullOrEmpty(string stringValue, string message)
        {
            if (string.IsNullOrEmpty(stringValue))
                _validationResult.AddError(message);
        }

        public static void AssertArgumentNotEquals(object object1, object object2, string message)
        {
            if (object1.Equals(object2))
                _validationResult.AddError(message);
        }

        public static void AssertArgumentNotNull(object object1, string message)
        {
            if (object1 == null)
                _validationResult.AddError(message);
        }

        public static void AssertArgumentRange(double value, double minimum, double maximum, string message)
        {
            if (value < minimum || value > maximum)
            {
                _validationResult.AddError(message);
            }
        }

        public static void AssertArgumentRange(float value, float minimum, float maximum, string message)
        {
            if (value < minimum || value > maximum)
            {
                _validationResult.AddError(message);
            }
        }

        public static void AssertArgumentRange(int value, int minimum, int maximum, string message)
        {
            if (value < minimum || value > maximum)
            {
                _validationResult.AddError(message);
            }
        }

        public static void AssertArgumentRange(long value, long minimum, long maximum, string message)
        {
            if (value < minimum || value > maximum)
            {
                _validationResult.AddError(message);
            }
        }

        public static void AssertArgumentTrue(bool boolValue, string message)
        {
            if (!boolValue)
                _validationResult.AddError(message);
        }

        public static void AssertStateFalse(bool boolValue, string message)
        {
            if (boolValue)
                _validationResult.AddError(message);
        }

        public static void AssertStateTrue(bool boolValue, string message)
        {
            if (!boolValue)
                _validationResult.AddError(message);
        }
    }
}