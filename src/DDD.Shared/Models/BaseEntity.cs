﻿using DDD.Shared.Interfaces;
using DDD.Shared.Validations;
using System;

namespace DDD.Shared.Models
{
    public abstract class BaseEntity : IAggregateRoot
    {
        public virtual Guid Id { get; protected set; }
        public bool IsDeleted { get; protected set; }

        protected BaseEntity()
        {
            IsDeleted = false;
        }

        public void SetAsDeleted()
        {
            this.IsDeleted = true;
        }

        public override bool Equals(object obj)
        {
            var compareTo = obj as BaseEntity;

            if (ReferenceEquals(this, compareTo))
                return true;

            if (ReferenceEquals(null, compareTo))
                return false;

            return Id.Equals(compareTo.Id);
        }

        public static bool operator == (BaseEntity a, BaseEntity b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator != (BaseEntity a, BaseEntity b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return (GetType().GetHashCode() * 1315) + Id.GetHashCode();
        }

        public override string ToString()
        {
            return GetType().Name + $"[Id = {Id}]";
        }
    }
}
