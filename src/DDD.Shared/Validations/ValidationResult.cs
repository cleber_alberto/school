﻿using DDD.Shared.Validations.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace DDD.Shared.Validations
{
    public class ValidationResult : IValidationResult
    {
        public string Message { get; set; }
        public List<ValidationError> Erros { get; } = new List<ValidationError>();

        public bool IsValid => !Erros.Any();

        public ValidationResult()
        {
        }

        public ValidationResult(string error)
        {
            AddError(error);
        }

        public void AddError(string error)
        {
            var validationError = new ValidationError(error);
            Erros.Add(validationError);
        }

        public void AddError(ValidationError error)
        {
            Erros.Add(error);
        }

        public void AddError(params ValidationResult[] resultadoValidacao)
        {
            if (resultadoValidacao == null) return;

            foreach (var validationResult in resultadoValidacao.Where(result => result != null))
                Erros.AddRange(validationResult.Erros);
        }

        public void RemoveError(ValidationError error)
        {
            if (Erros.Contains(error))
                Erros.Remove(error);
        }
    }
}