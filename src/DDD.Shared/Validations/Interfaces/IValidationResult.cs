﻿using System.Collections.Generic;

namespace DDD.Shared.Validations.Interfaces
{
    public interface IValidationResult
    {
        string Message { get; set; }
        bool IsValid { get; }
        List<ValidationError> Erros { get; }        

        void AddError(string error);
        void AddError(ValidationError error);
        void AddError(params ValidationResult[] resultadoValidacao);
        void RemoveError(ValidationError error);
    }
}