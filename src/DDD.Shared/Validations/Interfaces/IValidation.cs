﻿namespace DDD.Shared.Validations.Interfaces
{
    public interface IValidation<in TEntity>
    {
        ValidationResult GetValidationResult(TEntity entity);
    }
}