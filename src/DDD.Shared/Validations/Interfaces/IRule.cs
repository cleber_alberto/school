﻿namespace DDD.Shared.Validations.Interfaces
{
    public interface IRule<in TEntity>
    {
        string ErrorMessage { get; }
        bool Validate(TEntity entity);
    }
}