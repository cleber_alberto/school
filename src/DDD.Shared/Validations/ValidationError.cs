﻿namespace DDD.Shared.Validations
{
    public class ValidationError
    {
        public string PropertyName { get; set; }
        public string ErrorMessage { get; set; }

        protected internal ValidationError(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
    }
}