﻿using DDD.Shared.Interfaces;
using DDD.Shared.Validations.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace DDD.Shared.Validations
{
    public class Validation<TEntity> : IValidation<TEntity> where TEntity : class
    {
        private readonly Dictionary<string, IRule<TEntity>> _validations = new Dictionary<string, IRule<TEntity>>();

        protected virtual void Add(string ruleName, IRule<TEntity> rule)
        {
            _validations.Add(ruleName, rule);
        }

        protected virtual void Remove(string ruleName)
        {
            _validations.Remove(ruleName);
        }

        public virtual ValidationResult GetValidationResult(TEntity entity)
        {
            var result = new ValidationResult();
            foreach (var rule in _validations.Keys.Select(x => _validations[x]).Where(rule => !rule.Validate(entity)))
            {
                result.AddError(new ValidationError(rule.ErrorMessage));
            }

            return result;
        }

        protected IRule<TEntity> GetRule(string ruleName)
        {
            IRule<TEntity> rule;
            _validations.TryGetValue(ruleName, out rule);
            return rule;
        }
    }
}