﻿using AutoMapper;
using DDD.Shared.CQRS.Interfaces;
using DDD.Shared.Interfaces;
using DDD.Shared.Notifications.Interfaces;
using School.Application.Interfaces;
using School.Application.ViewModels;
using School.Domain.Models;
using School.Domain.Models.Specifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace School.Application.Services
{
    public class CourseService : Service, ICourseService
    {
        private readonly INotification _notification;
        private readonly IBus _bus;
        private readonly IMapper _mapper;
        private readonly IRepository<Course> _repository;


        public CourseService(INotification notification, IUnitOfWork uow, IBus bus, IMapper mapper, IRepository<Course> repository) 
            : base(notification, uow, bus, mapper)
        {
            _notification = notification;
            _bus = bus;
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<IEnumerable<CourseViewModel>> ListAsync()
        {
            var task = _repository.ListAsync(new CourseFilterSpecification());
            return _mapper.Map<IEnumerable<CourseViewModel>>(await task);
        }

        public async Task<CourseViewModel> GetAsync(Guid id)
        {
            var task = _repository.GetByIdAsync(id);
            return _mapper.Map<CourseViewModel>(await task);
        }

        public void Add(CourseViewModel model)
        {
            var course = Course.Create(model.Name);
            var validationResult = course.Validation();
            if (!validationResult.IsValid)
            {
                Notifications(validationResult);
                return;
            }

            _repository.Add(course);
            Commit();
        }

        public void Update(CourseViewModel model)
        {
            var course = _repository.GetById(model.Id);

            if (course == null)
            {
                _notification.Add("Course not found.");
                return;
            }

            course.Name = model.Name;
            var validationResult = course.Validation();
            if (!validationResult.IsValid)
            {
                Notifications(validationResult);
                return;
            }

            _repository.Update(course);
            Commit();
        }
        public void Remove(CourseViewModel model)
        {
            var course = _repository.GetById(model.Id);

            if (course == null)
            {
                _notification.Add("Course not found.");
                return;
            }

            course.SetAsDeleted();
            _repository.Update(course);
            Commit();
        }
    }
}
