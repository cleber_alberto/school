﻿using AutoMapper;
using DDD.Shared.CQRS.Interfaces;
using DDD.Shared.Interfaces;
using DDD.Shared.Notifications.Interfaces;
using School.Application.Interfaces;
using School.Application.ViewModels;
using School.Domain.Commands;
using School.Domain.Models;
using School.Domain.Models.Specifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace School.Application.Services
{
    public class TeacherService : Service, ITeacherService
    {
        private readonly INotification _notification;
        private readonly IBus _bus;
        private readonly IMapper _mapper;
        private readonly IRepository<Teacher> _repository;
        private readonly IRepository<Course> _repositoryCourse;

        public TeacherService(
            INotification notification, 
            IUnitOfWork uow, 
            IBus bus, 
            IMapper mapper, 
            IRepository<Teacher> repository,
            IRepository<Course> repositoryCourse) 
            : base(notification, uow, bus, mapper)
        {
            _notification = notification;
            _bus = bus;
            _mapper = mapper;
            _repository = repository;
            _repositoryCourse = repositoryCourse;
        }

        public async Task<IEnumerable<TeacherViewModel>> ListAsync()
        {
            var task = _repository
                .ListAsync(new TeacherFilterSpecification());
            return _mapper.Map<IEnumerable<TeacherViewModel>>(await task);
        }

        public async Task<TeacherViewModel> GetAsync(Guid id)
        {
            var task = _repository.GetByIdAsync(id);
            return _mapper.Map<TeacherViewModel>(await task);
        }

        public void Add(TeacherViewModel model)
        {
            _bus.SendCommands(new TeacherAddCommand() { Name = model.Name, Classroom = model.Classroom, CourseId = model.CourseId });
            if (ValidOpertaion())
                Commit();
        }

        public void Update(TeacherViewModel model)
        {
            _bus.SendCommands(new TeacherUpdateCommand() { Id = model.Id, Name = model.Name, Classroom = model.Classroom, CourseId = model.CourseId });
            if (ValidOpertaion())
                Commit();
        }

        public void Delete(TeacherViewModel model)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<CourseViewModel>> ListCourseAsync()
        {
            var task = _repositoryCourse.ListAsync(new CourseFilterSpecification());
            return _mapper.Map<IEnumerable<CourseViewModel>>(await task);
        }
    }
}
