﻿using AutoMapper;
using DDD.Shared.CQRS.Interfaces;
using DDD.Shared.Interfaces;
using DDD.Shared.Notifications.Interfaces;
using DDD.Shared.Validations;
using School.Application.Interfaces;

namespace School.Application.Services
{
    public class Service : IService
    {
        private readonly INotification _notification;
        private readonly IUnitOfWork _uow;
        private readonly IBus _bus;
        private readonly IMapper _mapper;

        public Service(INotification notification, IUnitOfWork uow, IBus bus, IMapper mapper)
        {
            _notification = notification;
            _uow = uow;
            _bus = bus;
            _mapper = mapper;
        }

        protected bool Commit()
        {
            if (_notification.HasNotifications) return false;

            var commandResponse = _uow.Commit();
            if (commandResponse.Success)
                return true;

            return false;
        }

        protected void Notifications(ValidationResult validationResult)
        {
            foreach (var error in validationResult.Erros)
            {
                _notification.Add(error.ErrorMessage);
            }
        }

        protected bool ValidOpertaion()
        {
            return (!_notification.HasNotifications);
        }

        public void Dispose()
        {
            _uow.Dispose();
        }
    }
}
