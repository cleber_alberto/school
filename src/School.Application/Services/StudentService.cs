﻿using AutoMapper;
using DDD.Shared.CQRS.Interfaces;
using DDD.Shared.Interfaces;
using DDD.Shared.Notifications.Interfaces;
using School.Application.Interfaces;
using School.Application.ViewModels;
using School.Domain.Commands;
using School.Domain.Models;
using School.Domain.Models.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Application.Services
{
    public class StudentService : Service, IStudentService
    {
        private readonly IBus _bus;
        private readonly IMapper _mapper;
        private readonly IRepository<Student> _repository;
        private readonly IRepository<Course> _repositoryCourse;

        public StudentService(
            INotification notification, 
            IUnitOfWork uow, 
            IBus bus, 
            IMapper mapper, 
            IRepository<Student> repository,
            IRepository<Course> repositoryCourse) 
            : base(notification, uow, bus, mapper)
        {
            _bus = bus;
            _mapper = mapper;
            _repository = repository;
            _repositoryCourse = repositoryCourse;
        }

        public async Task<IEnumerable<StudentViewModel>> ListAsync()
        {
            var task = _repository.ListAsync(new StudentFilterSpecification());
            return _mapper.Map<IEnumerable<StudentViewModel>>(await task);
        }

        public async Task<StudentViewModel> GetAsync(Guid id)
        {
            var task = _repository
                .ListAsync(new StudentFilterSpecification(id));
            return _mapper.Map<StudentViewModel>((await task).FirstOrDefault());
        }

        public void Add(StudentViewModel model)
        {
            _bus.SendCommands(new StudentAddCommand()
            {
                Name = model.Name,
                BirthDate = model.BirthDate,
                Address = model.Address,
                Email = model.Email,
                Courses = _mapper.Map<List<Course>>(model.Courses)
            });

            if (ValidOpertaion())
                Commit();
        }

        public void Update(StudentViewModel model)
        {
            _bus.SendCommands(new StudentUpdateCommand()
            {
                Id = model.Id,
                Name = model.Name,
                BirthDate = model.BirthDate,
                Address = model.Address,
                Email = model.Email,
                Courses = _mapper.Map<List<Course>>(model.Courses)
            });

            if (ValidOpertaion())
                Commit();
        }

        public async Task<IEnumerable<CourseViewModel>> ListCoursesAsync()
        {
            var task = _repositoryCourse.ListAsync(new CourseFilterSpecification());
            return _mapper.Map<IEnumerable<CourseViewModel>>(await task);
        }

        public async Task<CourseViewModel> GetCoursesAsync(Guid id)
        {
            var task = _repositoryCourse.GetByIdAsync(id);
            return _mapper.Map<CourseViewModel>(await task);
        }
    }
}
