﻿using DDD.Shared.Notifications.Interfaces;
using Microsoft.AspNetCore.Identity;
using School.Application.Interfaces;
using School.Application.ViewModels;
using School.Infra.CrossCutting.Identity.Models;
using System.Threading.Tasks;

namespace School.Application.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly INotification _notification;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public IdentityService(
            INotification notification, 
            UserManager<ApplicationUser> userManager, 
            SignInManager<ApplicationUser> signInManager)
        {
            _notification = notification;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task LoginAsync(LoginViewModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Email);

            if (user == null)
                user = await _userManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                _notification.Add("Invalid login attempt.");
                return;
            }

            var result = await _signInManager.PasswordSignInAsync(user, model.Password, true, false);

            if (result.Succeeded)
            {
                return;
            }

            if (result.IsLockedOut)
            {
                _notification.Add("Blocked user.");
            }

            if (result.IsNotAllowed)
            {
                _notification.Add("Access not allowed.");
            }

            if (!result.Succeeded)
            {
                _notification.Add("Invalid login attempt.");
            }
        }

        public async Task LogoutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task Register(RegisterViewModel model)
        {
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email, EmailConfirmed = true };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                return;
            }

            foreach (var error in result.Errors)
            {
                _notification.Add($"{error.Code} - {error.Description}");
            }
        }
    }
}
