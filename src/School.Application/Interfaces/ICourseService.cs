﻿using School.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace School.Application.Interfaces
{
    public interface ICourseService : IService
    {
        Task<IEnumerable<CourseViewModel>> ListAsync();
        Task<CourseViewModel> GetAsync(Guid id);
        void Add(CourseViewModel model);
        void Update(CourseViewModel model);
        void Remove(CourseViewModel model);
    }
}
