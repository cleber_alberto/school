﻿using School.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace School.Application.Interfaces
{
    public interface ITeacherService : IService
    {
        Task<IEnumerable<TeacherViewModel>> ListAsync();
        Task<TeacherViewModel> GetAsync(Guid id);
        void Add(TeacherViewModel model);
        void Update(TeacherViewModel model);
        void Delete(TeacherViewModel model);
        Task<IEnumerable<CourseViewModel>> ListCourseAsync();
    }
}
