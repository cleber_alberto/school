﻿using School.Application.ViewModels;
using System.Threading.Tasks;

namespace School.Application.Interfaces
{
    public interface IIdentityService
    {
        Task LoginAsync(LoginViewModel model);
        Task LogoutAsync();
        Task Register(RegisterViewModel model);
    }
}
