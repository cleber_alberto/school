﻿using School.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace School.Application.Interfaces
{
    public interface IStudentService : IService
    {
        Task<IEnumerable<StudentViewModel>> ListAsync();
        Task<StudentViewModel> GetAsync(Guid id);
        void Add(StudentViewModel model);
        void Update(StudentViewModel model);
        Task<IEnumerable<CourseViewModel>> ListCoursesAsync();
        Task<CourseViewModel> GetCoursesAsync(Guid id);
    }
}
