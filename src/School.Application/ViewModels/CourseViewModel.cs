﻿using System;
using System.ComponentModel.DataAnnotations;

namespace School.Application.ViewModels
{
    public class CourseViewModel
    {
        [ScaffoldColumn(false)]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
