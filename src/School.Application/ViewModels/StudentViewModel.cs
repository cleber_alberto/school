﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace School.Application.ViewModels
{
    public class StudentViewModel
    {
        [ScaffoldColumn(false)]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Birth date")]
        public DateTime BirthDate { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        public List<CourseViewModel> Courses { get; set; }

        public StudentViewModel()
        {
            Courses = new List<CourseViewModel>();
        }
    }
}
