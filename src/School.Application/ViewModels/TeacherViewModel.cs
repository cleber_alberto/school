﻿using System;
using System.ComponentModel.DataAnnotations;

namespace School.Application.ViewModels
{
    public class TeacherViewModel
    {
        [ScaffoldColumn(false)]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Classroom { get; set; }

        [Required]
        [Display(Name = "Course")]
        public Guid CourseId { get; set; }

        public CourseViewModel Course { get; set; }
    }
}
