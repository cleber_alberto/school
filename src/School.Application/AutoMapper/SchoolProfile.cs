﻿using AutoMapper;
using School.Application.ViewModels;
using School.Domain.Models;
using System.Linq;

namespace School.Application.AutoMapper
{
    public class SchoolProfile : Profile
    {
        public SchoolProfile()
        {
            CreateMap<Course, CourseViewModel>();
            CreateMap<CourseViewModel, Course>();

            CreateMap<Teacher, TeacherViewModel>()
                .ForMember(dst => dst.Course, opt => opt.MapFrom(src => src.Course));

            CreateMap<Student, StudentViewModel>()
                .ForMember(dst => dst.Courses, opt => opt.MapFrom(src => src.Courses.Select(c => c.Course)));
        }
    }
}
