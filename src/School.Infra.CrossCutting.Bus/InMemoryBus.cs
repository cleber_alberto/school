﻿using DDD.Shared.CQRS;
using DDD.Shared.CQRS.Interfaces;
using System;

namespace School.Infra.CrossCutting.Bus
{
    public sealed class InMemoryBus : IBus
    {
        private static IServiceProvider _services;

        public InMemoryBus(IServiceProvider services)
        {
            _services = services;
        }

        public void RaiseEvent<T>(T theEvent) where T : Event
        {
            Publish(theEvent);
        }

        public void SendCommand<T>(T theCommand) where T : Command
        {
            Publish(theCommand);
        }

        public void SendCommands<TCommand>(params TCommand[] commands) where TCommand : ICommand
        {
            throw new NotImplementedException();
        }

        public void RaiseEvents<TEvent>(params TEvent[] events) where TEvent : IEvent
        {
            throw new NotImplementedException();
        }

        public TResponse SendRequest<TResponse>(IRequest<TResponse> theRequest) where TResponse : IResponse
        {
            throw new NotImplementedException();
        }

        private static void Publish<T>(T domainEvent)
            where T : Message
        {
            if (_services == null)
                return;

            var obj = _services.GetService(typeof(IHandler<T>));

            ((IHandler<T>)obj).Handle(domainEvent);
        }        
    }
}
