﻿using DDD.Shared.Interfaces;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using School.Domain.Models;
using School.Infra.Data.Configurations;
using System;
using System.Collections.Generic;
using System.Text;

namespace School.Infra.Data.DbContexts
{
    public class SchoolDbContext : DbContext, IDbContext
    {
        public SchoolDbContext(DbContextOptions<SchoolDbContext> options) : base(options)
        {
        }

        public DbSet<Course> Course { get; set; }
        public DbSet<Student> Student { get; set; }
        public DbSet<Teacher> Teacher { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CourseConfiguration());
            modelBuilder.ApplyConfiguration(new TeacherConfiguration());
            modelBuilder.ApplyConfiguration(new StudentConfiguration());
            modelBuilder.ApplyConfiguration(new StudentCourseConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
