﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using School.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace School.Infra.Data.Configurations
{
    internal class StudentConfiguration : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.Property(p => p.Name)
                .HasColumnType("varchar(256)");

            builder.Property(p => p.Address)
                .HasColumnType("varchar(256)");

            builder.Property(p => p.Email)
                .HasColumnType("varchar(256)");

            builder.HasMany(hm => hm.Courses)
                .WithOne(wo => wo.Student)
                .HasForeignKey(fk => fk.StudentId);
        }
    }
}
