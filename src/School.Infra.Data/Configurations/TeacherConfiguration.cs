﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using School.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace School.Infra.Data.Configurations
{
    internal class TeacherConfiguration : IEntityTypeConfiguration<Teacher>
    {
        public void Configure(EntityTypeBuilder<Teacher> builder)
        {
            builder.Property(p => p.Name)
                .HasColumnType("varchar(254)");

            builder.HasOne(ho => ho.Course)
                .WithMany(wm => wm.Teachers)
                .HasForeignKey(fk => fk.CourseId);
        }
    }
}
