﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using School.Domain.Models;

namespace School.Infra.Data.Configurations
{
    internal class CourseConfiguration : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.Property(p => p.Name)
                .HasColumnType("varchar(254)");

            builder.HasMany(p => p.Students)
                .WithOne(wo => wo.Course)
                .HasForeignKey(fk => fk.CourseId);
        }
    }
}
