﻿using DDD.Shared.CQRS;
using DDD.Shared.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace School.Infra.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbContext _dbContext;

        public UnitOfWork(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class => _dbContext.Entry(entity);
        public DbSet<TEntity> Set<TEntity>() where TEntity : class => _dbContext.Set<TEntity>();

        public CommandResult Commit()
        {
            var rowsAffected = _dbContext.SaveChanges();
            return new CommandResult(rowsAffected > 0);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
